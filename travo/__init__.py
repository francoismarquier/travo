__version__ = "1.0.1dev"

from .gitlab import Forge, GitLab
from .assignment import Assignment
from .course import Course
from .homework import Homework
